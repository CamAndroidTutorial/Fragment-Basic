package com.bunhann.fragmentbasic;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements FragmentMenu.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("DEBUG", getResources().getConfiguration().orientation + "");

        if (savedInstanceState == null) {
            // Instance of first fragment
            FragmentMenu firstFragment = new FragmentMenu();
            FragmentManager fm = getSupportFragmentManager();
            // Add Fragment to FrameLayout (flContainer), using FragmentManager
            FragmentTransaction ft = fm.beginTransaction();// begin  FragmentTransaction
            ft.add(R.id.flContainer, firstFragment);                                // add    Fragment
            ft.commit();                                                            // commit FragmentTransaction
        }

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            FragmentDetail secondFragment = new FragmentDetail();
            Bundle args = new Bundle();
            args.putInt("position", 0);
            secondFragment.setArguments(args);
            // (1) Communicate with Fragment using Bundle
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft2 = fm.beginTransaction();// begin  FragmentTransaction
            ft2.add(R.id.flContainer2, secondFragment);                               // add    Fragment
            ft2.commit();                                                            // commit FragmentTransaction

            FragmentMenu firstFragment = new FragmentMenu();
            // Add Fragment to FrameLayout (flContainer), using FragmentManager
            FragmentTransaction ft = fm.beginTransaction();// begin  FragmentTransaction
            ft.replace(R.id.flContainer, firstFragment);
            ft.commit();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onMenuItemSelected(int position) {
        Toast.makeText(this, "Called By Fragment Menu: position - "+ position, Toast.LENGTH_SHORT).show();
        FragmentDetail fragmentDetail = new FragmentDetail();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragmentDetail.setArguments(args);

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flContainer2, fragmentDetail) // replace flContainer
                    //.addToBackStack(null)
                    .commit();
        }else{
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flContainer, fragmentDetail) // replace flContainer
                    .addToBackStack(null)
                    .commit();
        }
    }
}

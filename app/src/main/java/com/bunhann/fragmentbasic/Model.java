package com.bunhann.fragmentbasic;

public class Model {

    public static String[] fruitMenu = {
            "Apple",
            "Banana",
            "Cherry",
            "Dragon Fruit"
    };

    public static String[] fruitDetails = {
            "An apple is a sweet, edible fruit produced by an apple tree. Apple trees are cultivated worldwide as a fruit tree, and is the most widely grown species in the genus Malus",
            "A banana is an edible fruit – botanically a berry – produced by several kinds of large herbaceous flowering plants in the genus Musa. In some countries, bananas used for cooking may be called plantains, distinguishing them from dessert bananas.",
            "A cherry is the fruit of many plants of the genus Prunus, and is a fleshy drupe. The cherry fruits of commerce usually are obtained from cultivars of a limited number of species such as the sweet cherry and the sour cherry.",
            "A pitaya or pitahaya is the fruit of several cactus species indigenous to the Americas. Pitaya usually refers to fruit of the genus Stenocereus, while pitahaya or dragon fruit refers to fruit of the genus Hylocereus."
    };

}
